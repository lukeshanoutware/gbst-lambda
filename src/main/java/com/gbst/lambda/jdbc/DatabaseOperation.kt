package com.gbst.lambda.jdbc

import java.sql.PreparedStatement

interface DatabaseOperation {

    fun sql(): String

    @Throws(Exception::class)
    fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult
}
