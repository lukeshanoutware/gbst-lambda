package com.gbst.lambda.jdbc

import com.gbst.lambda.model.ErrorResult
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement

object JDBCUtil {

    private val theConnection: Connection by lazy {
        getConnection()
    }

    @Throws(Exception::class)
    private fun getConnection(): Connection {
        val driver = "oracle.jdbc.driver.OracleDriver"
        val url = "jdbc:oracle:thin:@" + System.getenv("DB_ADDRESS") + ":1521:" + Constants.DB_NAME
        println(url)
        Class.forName(driver)
        return DriverManager.getConnection(url, Constants.USER, Constants.PASSWORD)
    }

    @Throws(Exception::class)
    private fun getPreparedStatement(connection: Connection, sql: String): PreparedStatement {
        return connection.prepareStatement(sql)
    }

    fun executeOperation(databaseOperation: DatabaseOperation): DatabaseOperationResult = executeOperationReuseConnection(databaseOperation)

    private fun executeOperationReuseConnection(databaseOperation: DatabaseOperation): DatabaseOperationResult {
        var preparedStatement: PreparedStatement? = null
        return try {
            preparedStatement = getPreparedStatement(theConnection, databaseOperation.sql())
            databaseOperation.execute(preparedStatement)
        } catch (e: Exception) {
            ErrorResult(e)
        } finally {
            preparedStatement?.close()
            println("JDBC PreparedStatement closed")
        }
    }

    private fun executeOperationNewConnection(databaseOperation: DatabaseOperation): DatabaseOperationResult {
        var connection: Connection? = null
        var preparedStatement: PreparedStatement? = null
        return try {
            connection = getConnection()
            preparedStatement = getPreparedStatement(connection, databaseOperation.sql())
            databaseOperation.execute(preparedStatement)
        } catch (e: Exception) {
            ErrorResult(e)
        } finally {
            preparedStatement?.close()
            connection?.close()
            println("JDBC PreparedStatement & Connection closed")
        }
    }
}
