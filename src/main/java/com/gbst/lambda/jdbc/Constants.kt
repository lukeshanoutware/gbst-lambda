package com.gbst.lambda.jdbc

object Constants {
    private const val DB_ADDRESS_AWS = "172.31.3.40"
    private const val DB_ADDRESS_EXTERNAL = "syn32rb.gbst.com"
    const val DB_ADDRESS = DB_ADDRESS_EXTERNAL
    const val DB_NAME = "GBSTDEMO"
    const val USER = "SYNDEMO"
    const val PASSWORD = "SYNDEMO"
}
