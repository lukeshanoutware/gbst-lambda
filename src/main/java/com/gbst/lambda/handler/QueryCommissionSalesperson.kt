package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.*
import java.math.BigDecimal
import java.sql.PreparedStatement

/**
 * For conversation 6.4.1.a
 */
class QueryCommissionSalesperson : BaseHandler() {

    companion object {
        const val INTENT_NAME = "commQuerySalesperson"
        const val INCLUDE_ALL = """_ALL_"""

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = QueryCommissionSalesperson()
            val slots = mutableMapOf<String, String?>()
            slots[JoeSlot.SALESPERSON_STRING.slotName] = "sAlLy"
            val currentIntent = Intent(INTENT_NAME, slots, null, ConfirmationStatus.None)
            val lexRequest = LexRequest(currentIntent = currentIntent)
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        val salesperson = lexRequest.currentIntent.slots?.let { it[JoeSlot.SALESPERSON_STRING.slotName] } ?: INCLUDE_ALL
        JDBCUtil.executeOperation(DoQueryCommissionSalesperson(salesperson)).let {
            return when (it) {
                is CommissionSalesperson ->
                    closeResponse(lexRequest, it, salesperson)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, commissionSalesperson: CommissionSalesperson, salesperson: String): LexResponse {
        println(commissionSalesperson)
        val message = if (salesperson != INCLUDE_ALL) {
            """${commissionSalesperson.salesperson}'s commission for today is $${commissionSalesperson.dollars}."""
        } else {
            """All sales persons' total commission for today is $${commissionSalesperson.dollars}."""
        }
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoQueryCommissionSalesperson(private val salesperson: String) : DatabaseOperation {

        override fun sql(): String {
            return """with slots as (select ? salesperson, '_ALL_' includeall from dual),
                    | dates as (select max(asatdate) today from workzone_vw)
                    | select slots.salesperson, coalesce(sum(t.totalAbsCommission), 0) dollars
                    | from slots slots
                    | cross join dates dates
                    | inner join tradereport_vw t on t.creationasatdate = dates.today
                    | where t.relatedObjectType = 'ClientTrade' and t.chargecommccy='AUD' and t.tradeStatus not in ('Cancelled', 'Reversed', 'Transformed')
                    | and (slots.salesperson = slots.includeall or Upper(t.salesperson) = Upper(slots.salesperson))
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            preparedStatement.setString(1, salesperson)
            val resultSet = preparedStatement.executeQuery()
            return if (resultSet.next()) {
                CommissionSalesperson(resultSet.getString(1), resultSet.getBigDecimal(2))
            } else {
                throw Exception("Cannot find $salesperson's commission for today.")
            }
        }
    }

    /**
     * The data model that database operation returns
     */
    data class CommissionSalesperson(val salesperson: String = "", val dollars: BigDecimal = BigDecimal.ZERO) : DatabaseOperationResult()
}
