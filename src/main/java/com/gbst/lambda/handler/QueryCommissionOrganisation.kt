package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.*
import java.math.BigDecimal
import java.sql.PreparedStatement

/**
 * For conversation 6.4.1.b
 */
class QueryCommissionOrganisation : BaseHandler() {

    companion object {
        const val INTENT_NAME = "commQueryOrg"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = QueryCommissionOrganisation()
            val slots = mutableMapOf<String, String?>()
            slots[JoeSlot.CLIENT_STRING.slotName] = "RBAS-A006430"
            val currentIntent = Intent(INTENT_NAME, slots, null, ConfirmationStatus.None)
            val lexRequest = LexRequest(currentIntent = currentIntent)
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        val nextRequiredSlot = checkSlots(lexRequest, JoeSlot.CLIENT_STRING)
        return if (nextRequiredSlot != null) {
            slotResponse(lexRequest, nextRequiredSlot)
        } else {
            tryFulfillRequest(lexRequest)
        }
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        val client = lexRequest.currentIntent.slots?.let { it[JoeSlot.CLIENT_STRING.slotName] }!!
        JDBCUtil.executeOperation(DoQueryCommissionOrganisation(client)).let {
            return when (it) {
                is CommissionOrganisation ->
                    closeResponse(lexRequest, it)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, commissionOrganisation: CommissionOrganisation): LexResponse {
        println(commissionOrganisation)
        val message = """${commissionOrganisation.client}'s commission for today is $${commissionOrganisation.dollars}."""
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoQueryCommissionOrganisation(private val client: String) : DatabaseOperation {

        override fun sql(): String {
            return """with dates as (select max(asatdate) today from workzone_vw)
                    | select coalesce(c.partyshortname, c.partyreference) client, coalesce(sum(t.totalAbsCommission), 0) dollars
                    | from dates dates
                    | inner join tradereport_vw t on t.creationasatdate >= dates.today
                    | left outer join PARTYREPORT_ORGANISATION_VW c on c.PARTYREFERENCE = t.counterpartyreference
                    | where t.relatedObjectType = 'ClientTrade' and t.chargecommccy='AUD' and t.tradeStatus not in ('Cancelled', 'Reversed', 'Transformed')
                    | and UPPER(t.counterpartyreference) = UPPER(?)
                    | group by coalesce(c.partyshortname, c.partyreference)
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            preparedStatement.setString(1, client)
            val resultSet = preparedStatement.executeQuery()
            return if (resultSet.next()) {
                CommissionOrganisation(resultSet.getString(1), resultSet.getBigDecimal(2))
            } else {
                throw Exception("Cannot find $client's commission for today.")
            }
        }
    }

    /**
     * The data model that database operation returns
     */
    data class CommissionOrganisation(val client: String = "", val dollars: BigDecimal = BigDecimal.ZERO) : DatabaseOperationResult()
}
