package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.gbst.lambda.model.FulfillmentState
import com.gbst.lambda.model.JoeSlot
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse
import java.io.InputStream
import java.io.OutputStream

abstract class BaseHandler : RequestStreamHandler {

    abstract fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse

    override fun handleRequest(inputStream: InputStream, outputStream: OutputStream, context: Context) {
        val mapper = jacksonObjectMapper()
        val lexRequest = mapper.readValue<LexRequest>(inputStream)
        val lexResponse = processRequest(lexRequest, context)
        mapper.writeValue(outputStream, lexResponse)
    }

    /**
     * return next required slot name. If all slots have value return null.
     */
    protected fun checkSlots(lexRequest: LexRequest, vararg requiredSlots: JoeSlot): JoeSlot? =
            lexRequest.currentIntent.slots?.let { currentSlots ->
                requiredSlots.firstOrNull {
                    currentSlots[it.slotName] == null
                }
            }

    /**
     * Ask user to provide a slot value
     */
    protected fun slotResponse(lexRequest: LexRequest, nextRequiredSlot: JoeSlot): LexResponse {
        return LexResponse.createSlotResponse(lexRequest, nextRequiredSlot.message, nextRequiredSlot.slotName)
    }

    /**
     * Something went wrong...
     */
    protected fun errorResponse(lexRequest: LexRequest, exception: Exception? = null): LexResponse =
            LexResponse.createCloseResponse(lexRequest, FulfillmentState.Failed, exception?.message ?: "Sorry, something went wrong. Please try again")

}