package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.*
import java.sql.PreparedStatement

/**
 * For conversation 4.4.1
 */
class CheckOpenTrades : BaseHandler() {

    companion object {
        const val INTENT_NAME = "openTradesCheck"
        const val INCLUDE_ALL = """_ALL_"""

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = CheckOpenTrades()
            val slots = mutableMapOf<String, String?>()
            slots[JoeSlot.TRADE_STATUS_STRING.slotName] = "Rejected"
            slots[JoeSlot.ETC_PROTOCOL_STRING.slotName] = "IRESS"
            slots[JoeSlot.MARKET_STRING.slotName] = "XNAS"
            val currentIntent = Intent(INTENT_NAME, slots, null, ConfirmationStatus.None)
            val lexRequest = LexRequest(currentIntent = currentIntent)
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        val tradeStatus = lexRequest.currentIntent.slots?.let { it[JoeSlot.TRADE_STATUS_STRING.slotName] } ?: INCLUDE_ALL
        val etcProtocol = lexRequest.currentIntent.slots?.let { it[JoeSlot.ETC_PROTOCOL_STRING.slotName] } ?: INCLUDE_ALL
        val market = lexRequest.currentIntent.slots?.let { it[JoeSlot.MARKET_STRING.slotName] } ?: INCLUDE_ALL
        JDBCUtil.executeOperation(DoCheckOpenTrades(tradeStatus, etcProtocol, market)).let {
            return when (it) {
                is OpenTrades ->
                    closeResponse(lexRequest, it, tradeStatus, etcProtocol, market)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, openTrades: OpenTrades, tradeStatus: String, etcProtocol: String, market: String): LexResponse {
        println(openTrades)
        val conditionString: String =
                if (tradeStatus != INCLUDE_ALL) {
                    tradeStatus
                } else {
                    """"""
                }.let { statusString ->
                    if (etcProtocol != INCLUDE_ALL) {
                        if (statusString.isNotEmpty()) {
                            """$statusString $etcProtocol"""
                        } else {
                            etcProtocol
                        }
                    } else {
                        statusString
                    }.let { statusAndProtocolString ->
                        if (market != INCLUDE_ALL) {
                            if (statusAndProtocolString.isNotEmpty()) {
                                """$statusAndProtocolString trades in <say-as interpret-as="spell-out">$market</say-as>"""
                            } else {
                                """trades in <say-as interpret-as="spell-out">$market</say-as>"""
                            }
                        } else {
                            if (statusAndProtocolString.isNotEmpty()) {
                                """$statusAndProtocolString trades"""
                            } else {
                                """trades"""
                            }
                        }
                    }
                }
        val message = openTrades.items.filter {
            it.count > 0
        }.map {
            when (it.count) {
                1 -> """${it.count} trade for ${it.client}"""
                else -> """${it.count} trades for ${it.client}"""
            }
        }.takeIf {
            it.isNotEmpty()
        }?.let { mismatchList ->
            if (openTrades.items[0].otherTrades > 0) {
                mismatchList.joinToString(", ").let {
                    val mismatches = when (openTrades.items[0].otherTrades) {
                        1 -> """${openTrades.items[0].otherTrades} trade"""
                        else -> """${openTrades.items[0].otherTrades} trades"""
                    }
                    val organisations = when (openTrades.items[0].otherOrgs) {
                        1 -> """${openTrades.items[0].otherOrgs} other organisation"""
                        else -> """${openTrades.items[0].otherOrgs} other organisations"""
                    }
                    """The $conditionString are: $it and $mismatches for $organisations."""
                }
            } else {
                val lastItemIndex = mismatchList.lastIndex
                mismatchList.foldIndexed("""The $conditionString are:"""
                ) { index, accumulator, item ->
                    when (index) {
                        0 -> if (0 != lastItemIndex) {
                            "$accumulator $item"
                        } else {
                            "$accumulator $item."
                        }
                        lastItemIndex -> "$accumulator and $item."
                        else -> "$accumulator, $item"
                    }
                }
            }
        } ?: """You have no outstanding $conditionString as of now."""
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoCheckOpenTrades(private val tradeStatus: String, private val etcProtocol: String, private val market: String) : DatabaseOperation {

        override fun sql(): String {
            return """with slots as (select ? status, ? etcprotocol, ? market, '_ALL_' includeall from dual)
                    | select orgtotal as "Count", coalesce(c.partyshortname, c.partyreference) as "Client", total - sum(orgtotal) over () as "other trades", numorgs - count(*) over () as "other orgs" from
                    | (select etcorganisationuniqueid, max(orgtotal) orgtotal, max(total) total, count(*) over () numorgs from
                    | (select etcorganisationuniqueid, count(*) over (partition by etcorganisationuniqueid) orgtotal, count(*) over () as total
                    | from slots slots
                    | cross join ETCMonitorView_VW
                    | where etcSuperStatus = 'Current'
                    | and etcblocktype != 'AE'
                    | and (slots.status = slots.includeall or UPPER(etcblockstatus) = UPPER(slots.status))
                    | and (slots.etcprotocol = slots.includeall or UPPER(etcprovidername) = UPPER(slots.etcprotocol))
                    | and (slots.market = slots.includeall or UPPER(etcmarketconventionid) = UPPER(slots.market)))
                    | group by etcorganisationuniqueid
                    | order by orgtotal desc)
                    | left outer join PARTYREPORT_ORGANISATION_VW c on c.partyreference = etcorganisationuniqueid and isclientrole = 'Y'
                    | where ROWNUM <= 3
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val openTrades = OpenTrades()
            preparedStatement.setString(1, tradeStatus)
            preparedStatement.setString(2, etcProtocol)
            preparedStatement.setString(3, market)
            val resultSet = preparedStatement.executeQuery()
            while (resultSet.next()) {
                openTrades.addItem(
                        OpenTradesItem(resultSet.getInt("Count"),
                                resultSet.getString("Client"),
                                resultSet.getInt("other trades"),
                                resultSet.getInt("other orgs")))
            }
            return openTrades
        }
    }

    /**
     * The data model that database operation returns
     */
    data class OpenTradesItem(val count: Int = 0, val client: String = "", val otherTrades: Int = 0, val otherOrgs: Int = 0) : DatabaseOperationResult()

    data class OpenTrades(val items: MutableList<OpenTradesItem> = mutableListOf()) : DatabaseOperationResult() {
        fun addItem(openTradesItem: OpenTradesItem) {
            items.add(openTradesItem)
        }
    }
}
