package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.ErrorResult
import com.gbst.lambda.model.FulfillmentState
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse
import java.sql.PreparedStatement

/**
 * For conversation 2.4.2
 */
class CheckBacklogOfExecutions : BaseHandler() {

    companion object {
        const val INTENT_NAME = "sysStatusBacklogExecutionCheck"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = CheckBacklogOfExecutions()
            val lexRequest = LexRequest()
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        JDBCUtil.executeOperation(DoCheckBacklogOfExecutions()).let {
            return when (it) {
                is BacklogOfExecutions ->
                    closeResponse(lexRequest, it)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, backlogOfExecutions: BacklogOfExecutions): LexResponse {
        println(backlogOfExecutions)
        val message = when (backlogOfExecutions.count) {
            0 -> """All executions are booked."""
            1 -> """There is ${backlogOfExecutions.count} unbooked execution."""
            else -> """There are ${backlogOfExecutions.count} unbooked executions."""
        }
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoCheckBacklogOfExecutions : DatabaseOperation {

        override fun sql(): String {
            return """select count(*) "backlog of executions" from MKTEXECLEG_TB where PRIORITY = 1 and (status = 'Open' or status = 'Reversal')""".trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val resultSet = preparedStatement.executeQuery()
            return if (resultSet.next()) {
                BacklogOfExecutions(resultSet.getInt(1))
            } else {
                throw Exception("Cannot get backlog of executions from database.")
            }
        }
    }

    /**
     * The data model that database operation returns
     */
    data class BacklogOfExecutions(val count: Int = 0) : DatabaseOperationResult()
}
