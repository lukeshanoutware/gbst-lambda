package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.ErrorResult
import com.gbst.lambda.model.FulfillmentState
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse
import java.sql.PreparedStatement

/**
 * For conversation 2.4.1
 */
class CheckBacklogOfETCAllocations : BaseHandler() {

    companion object {
        const val INTENT_NAME = "sysStatusBacklogAllocCheck"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = CheckBacklogOfETCAllocations()
            val lexRequest = LexRequest()
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        JDBCUtil.executeOperation(DoCheckBacklogOfETCAllocations()).let {
            return when (it) {
                is BacklogOfETCAllocations ->
                    closeResponse(lexRequest, it)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, backlogOfETCAllocations: BacklogOfETCAllocations): LexResponse {
        println(backlogOfETCAllocations)
        val message = when (backlogOfETCAllocations.count) {
            0 -> """All allocations are processed."""
            1 -> """There is ${backlogOfETCAllocations.count} outstanding ETC allocation."""
            else -> """There are ${backlogOfETCAllocations.count} outstanding ETC allocations."""
        }
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoCheckBacklogOfETCAllocations : DatabaseOperation {

        override fun sql(): String {
            return """select count(*) "backlog of allocations" from alloctdetail_tb where ETCBLKSTAT = 'Matched' and row_index = 0""".trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val resultSet = preparedStatement.executeQuery()
            return if (resultSet.next()) {
                BacklogOfETCAllocations(resultSet.getInt(1))
            } else {
                throw Exception("Cannot get backlog of ETC allocations from database.")
            }
        }
    }

    /**
     * The data model that database operation returns
     */
    data class BacklogOfETCAllocations(val count: Int = 0) : DatabaseOperationResult()
}
