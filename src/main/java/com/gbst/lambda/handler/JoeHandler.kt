package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse

class JoeHandler : BaseHandler() {
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        return when (lexRequest.currentIntent.name) {
            //2.4.1
            CheckBacklogOfETCAllocations.INTENT_NAME ->
                CheckBacklogOfETCAllocations().processRequest(lexRequest, context)
            //2.4.2
            CheckBacklogOfExecutions.INTENT_NAME ->
                CheckBacklogOfExecutions().processRequest(lexRequest, context)
            //3.4.1
            CheckExecutionsReceived.INTENT_NAME ->
                CheckExecutionsReceived().processRequest(lexRequest, context)
            //4.4.1
            CheckOpenTrades.INTENT_NAME ->
                CheckOpenTrades().processRequest(lexRequest, context)
            //5.4.1
            QueryCommissionRateFund.INTENT_NAME ->
                QueryCommissionRateFund().processRequest(lexRequest, context)
            //5.4.2
            FindTradeAccounts.INTENT_NAME ->
                FindTradeAccounts().processRequest(lexRequest, context)
            //6.4.1.a
            QueryCommissionSalesperson.INTENT_NAME ->
                QueryCommissionSalesperson().processRequest(lexRequest, context)
            //6.4.1.b
            QueryCommissionOrganisation.INTENT_NAME ->
                QueryCommissionOrganisation().processRequest(lexRequest, context)
            // Heartbeat
            Heartbeat.INTENT_NAME ->
                Heartbeat().processRequest(lexRequest, context)
            else ->
                errorResponse(lexRequest, Exception("Sorry, I don't know how to handle intent [${lexRequest.currentIntent.name}]. Please inform the development team about this particular scenario."))
        }
    }
}