package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.ErrorResult
import com.gbst.lambda.model.FulfillmentState
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse
import java.sql.PreparedStatement
import java.util.*

/**
 * For conversation 3.4.1
 */
class CheckExecutionsReceived : BaseHandler() {

    companion object {
        const val INTENT_NAME = "txnThroughputExecutionsCheck"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = CheckExecutionsReceived()
            val lexRequest = LexRequest()
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        JDBCUtil.executeOperation(DoCheckExecutionsReceived()).let {
            return when (it) {
                is ExecutionsReceived ->
                    closeResponse(lexRequest, it)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, executionsReceived: ExecutionsReceived): LexResponse {
        println(executionsReceived)
        val message = when (executionsReceived.count) {
            0 -> """No executions have been received on ${executionsReceived.date}."""
            1 -> """${executionsReceived.count} execution has been received on ${executionsReceived.date}."""
            else -> """${executionsReceived.count} executions have been received on ${executionsReceived.date}."""
        }
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoCheckExecutionsReceived : DatabaseOperation {

        override fun sql(): String {
            return """with dates as (select max(asatdate) today from workzone_vw)
                    | select (select count(*) from MKTEXECLEG_TB where tradedate = today) as "number of executions", today as "today"
                    | from dates
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val resultSet = preparedStatement.executeQuery()
            return if (resultSet.next()) {
                ExecutionsReceived(resultSet.getInt(1), resultSet.getDate(2))
            } else {
                throw Exception("Cannot get executions received today from database.")
            }
        }
    }

    /**
     * The data model that database operation returns
     */
    data class ExecutionsReceived(val count: Int = 0, val date: Date = Date()) : DatabaseOperationResult()
}
