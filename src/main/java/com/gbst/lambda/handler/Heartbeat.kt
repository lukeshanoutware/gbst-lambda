package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.model.FulfillmentState
import com.gbst.lambda.model.LexRequest
import com.gbst.lambda.model.LexResponse

/**
 * Heartbeat
 */
class Heartbeat : BaseHandler() {

    companion object {
        const val INTENT_NAME = "Heartbeat"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = Heartbeat()
            val lexRequest = LexRequest()
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        return tryFulfillRequest(lexRequest)
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        return closeResponse(lexRequest)
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest): LexResponse {
        val message = """I am alive."""
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }
}
