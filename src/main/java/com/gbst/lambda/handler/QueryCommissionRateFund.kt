package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.*
import java.sql.PreparedStatement

/**
 * For conversation 5.4.1
 */
class QueryCommissionRateFund : BaseHandler() {

    companion object {
        const val INTENT_NAME = "sysDataReqFundComm"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = QueryCommissionRateFund()
            val slots = mutableMapOf<String, String?>()
            slots[JoeSlot.FUND_ID_STRING.slotName] = "WIL001"
            val currentIntent = Intent(INTENT_NAME, slots, null, ConfirmationStatus.None)
            val lexRequest = LexRequest(currentIntent = currentIntent)
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        val nextRequiredSlot = checkSlots(lexRequest, JoeSlot.FUND_ID_STRING)
        return if (nextRequiredSlot != null) {
            slotResponse(lexRequest, nextRequiredSlot)
        } else {
            tryFulfillRequest(lexRequest)
        }
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        val fundId = lexRequest.currentIntent.slots?.let { it[JoeSlot.FUND_ID_STRING.slotName] }!!
        JDBCUtil.executeOperation(DoQueryCommissionRateFund(fundId)).let {
            return when (it) {
                is FundCommissionRates ->
                    closeResponse(lexRequest, it, fundId)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, fundCommissionRates: FundCommissionRates, fundId: String): LexResponse {
        println(fundCommissionRates)
        val message = fundCommissionRates.items.map {
            """The commission rate on fund id <say-as interpret-as="spell-out">${it.fundId}</say-as> for ${it.client} is ${it.bps} <phoneme alphabet="ipa" ph="bɪps">BPS</phoneme> and it is ${if (it.GST == "Y") "" else "not "}GST payable."""
        }.takeIf {
            it.isNotEmpty()
        }?.let { commissionRateList ->
            commissionRateList.joinToString(" ")
        } ?: """I cannot find the commission rate for fund id <say-as interpret-as="spell-out">$fundId</say-as>."""
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoQueryCommissionRateFund(private val fundId: String) : DatabaseOperation {

        override fun sql(): String {
            return """with clients as
                    | (select distinct p.parentpartyref, ref.creference fundid, ref.tradaccid, coalesce(c.partyshortname, c.partyreference) client, c.country
                    | from corprovacc_tb ref
                    | inner join partyreport_tradingaccount_vw p on p.partyreference = ref.tradaccid and p.OWNERCLIENT = 'Y'
                    | left outer join PARTYREPORT_ORGANISATION_VW c on c.PARTYREFERENCE = p.PARENTPARTYREF
                    | where UPPER(ref.creference) = UPPER(?)
                    | ),
                    | accounts as (
                    | select t.relatedobjectooid
                    | from clients c
                    | inner join partyreport_tradingaccount_vw t on t.parentpartyref = c.parentpartyref
                    | inner join accttype_tb acttype on acttype.name = t.accounttypename
                    | where (t.partyreference = c.tradaccid or acttype.isblock = 'Y')
                    | ),
                    | charges as
                    | (
                    | select c.cptyuniqueid, coalesce(c.bpsrate, c.percentage * 100) bps, c.thertradacct, c.tradaccname, c.chargescal, t.ooid,
                    | (case when c.allocchrg = 'Yes' and c.thertradacct is not null then 1
                    |         when c.thertradacct is not null then 2
                    |         else 3
                    | end) priority, t.accounttype, t.accounttypename, acttype.isblock
                    | from chargerule_tb c
                    | left outer join partyreport_tradingaccount_vw t on t.relatedobjectooid = c.thertradacct
                    | left outer join accttype_tb acttype on acttype.name = t.accounttypename
                    | where c.CCODENAM = 'COMM'
                    | and c.ACTIVE = 'Y'
                    | and c.cptyuniqueid is not null
                    | and c.chargescal != 'ProRate'
                    | order by priority
                    | )
                    | select fundid, client, bps, GST from (
                    | select client.fundid, client.client, charges.*,
                    | row_number() over (partition by charges.cptyuniqueid order by priority) perclientpriority,
                    | (case when client.country = 'AU' then 'Y' else 'N' end) GST
                    | from clients client
                    | inner join charges charges on charges.cptyuniqueid = client.parentpartyref
                    | where (charges.thertradacct in (select relatedobjectooid from accounts) or charges.thertradacct is null)
                    | order by priority
                    | )
                    | where perclientpriority = 1
                    | order by cptyuniqueid, priority
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val fundCommissionRates = FundCommissionRates()
            preparedStatement.setString(1, fundId)
            val resultSet = preparedStatement.executeQuery()
            while (resultSet.next()) {
                fundCommissionRates.addItem(
                        FundCommissionRateItem(resultSet.getString("fundid"),
                                resultSet.getString("client"),
                                resultSet.getInt("bps"),
                                resultSet.getString("GST")))
            }
            return fundCommissionRates
        }
    }

    /**
     * The data model that database operation returns
     */
    data class FundCommissionRateItem(val fundId: String = "", val client: String = "", val bps: Int = 0, val GST: String = "Y") : DatabaseOperationResult()

    data class FundCommissionRates(val items: MutableList<FundCommissionRateItem> = mutableListOf()) : DatabaseOperationResult() {
        fun addItem(fundCommissionRateItem: FundCommissionRateItem) {
            items.add(fundCommissionRateItem)
        }
    }
}
