package com.gbst.lambda.handler

import com.amazonaws.services.lambda.runtime.Context
import com.gbst.lambda.jdbc.DatabaseOperation
import com.gbst.lambda.jdbc.DatabaseOperationResult
import com.gbst.lambda.jdbc.JDBCUtil
import com.gbst.lambda.model.*
import java.sql.PreparedStatement

/**
 * For conversation 5.4.2
 */
class FindTradeAccounts : BaseHandler() {

    companion object {
        const val INTENT_NAME = "sysDataReqTradingAccount"

        /**
         * To test the function locally
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val handler = FindTradeAccounts()
            val slots = mutableMapOf<String, String?>()
            slots[JoeSlot.FUND_ID_STRING.slotName] = "ASA001"
            val currentIntent = Intent(INTENT_NAME, slots, null, ConfirmationStatus.None)
            val lexRequest = LexRequest(currentIntent = currentIntent)
            val lexResponse: LexResponse = handler.processRequest(lexRequest, null)
            println("$lexResponse")
        }
    }

    /**
     * Entry of processing
     */
    override fun processRequest(lexRequest: LexRequest, context: Context?): LexResponse {
        println(lexRequest.toString())
        val nextRequiredSlot = checkSlots(lexRequest, JoeSlot.FUND_ID_STRING)
        return if (nextRequiredSlot != null) {
            slotResponse(lexRequest, nextRequiredSlot)
        } else {
            tryFulfillRequest(lexRequest)
        }
    }

    /**
     * Query database and conduct response
     */
    private fun tryFulfillRequest(lexRequest: LexRequest): LexResponse {
        val fundId = lexRequest.currentIntent.slots?.let { it[JoeSlot.FUND_ID_STRING.slotName] }!!
        JDBCUtil.executeOperation(DoFindTradeAccounts(fundId)).let {
            return when (it) {
                is TradeAccounts ->
                    closeResponse(lexRequest, it, fundId)
                else ->
                    errorResponse(lexRequest, (it as ErrorResult).exception)
            }
        }
    }

    /**
     * Send a success message to user
     */
    private fun closeResponse(lexRequest: LexRequest, tradeAccounts: TradeAccounts, fundId: String): LexResponse {
        println(tradeAccounts)
        val message = tradeAccounts.items.map {
            """${it.tradingAccount} with ${it.allocationProvider} for ${it.client}"""
        }.takeIf {
            it.isNotEmpty()
        }?.let { accountList ->
            if (accountList.size == 1) {
                """The trading account for <say-as interpret-as="spell-out">$fundId</say-as> is ${accountList.first()}."""
            } else {
                """The trading accounts for <say-as interpret-as="spell-out">$fundId</say-as> are ${accountList.joinToString(", ")}."""
            }
        } ?: """There are no trading accounts for fund id <say-as interpret-as="spell-out">$fundId</say-as>."""
        return LexResponse.createCloseResponse(lexRequest, FulfillmentState.Fulfilled, message)
    }

    /**
     * The SQL query is here
     */
    class DoFindTradeAccounts(private val fundId: String) : DatabaseOperation {

        override fun sql(): String {
            return """select ref.creference as "Fund ID", ref.allocprovid as "Allocation Provider", coalesce(p.partyshortname, p.partyreference, ref.TRADACCID) as "Trading Account", coalesce(c.partyshortname, c.partyreference) "Client" from corprovacc_tb ref
                    | left outer join partyreport_tradingaccount_vw p on p.partyreference = ref.tradaccid and p.OWNERCLIENT = 'Y'
                    | left outer join PARTYREPORT_ORGANISATION_VW c on c.PARTYREFERENCE = p.PARENTPARTYREF
                    | where ref.creference = ?
                    | order by client
                    | """.trimMargin()
        }

        @Throws(Exception::class)
        override fun execute(preparedStatement: PreparedStatement): DatabaseOperationResult {
            val tradeAccounts = TradeAccounts()
            preparedStatement.setString(1, fundId)
            val resultSet = preparedStatement.executeQuery()
            while (resultSet.next()) {
                tradeAccounts.addItem(
                        TradeAccountItem(resultSet.getString("Fund ID"),
                                resultSet.getString("Allocation Provider"),
                                resultSet.getString("Trading Account"),
                                resultSet.getString("Client")))
            }
            return tradeAccounts
        }
    }

    /**
     * The data model that database operation returns
     */
    data class TradeAccountItem(val fundId: String = "", val allocationProvider: String = "", val tradingAccount: String = "", val client: String = "") : DatabaseOperationResult()

    data class TradeAccounts(val items: MutableList<TradeAccountItem> = mutableListOf()) : DatabaseOperationResult() {
        fun addItem(tradeAccountItem: TradeAccountItem) {
            items.add(tradeAccountItem)
        }
    }
}
