package com.gbst.lambda.model

data class DialogActionElicitIntent(var message: Message? = null,
                                    var responseCard: ResponseCard? = null) : DialogAction(Type.ElicitIntent)