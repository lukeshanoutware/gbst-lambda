package com.gbst.lambda.model

enum class JoeSlot(val slotName: String, val message: String) {
    FUND_ID_STRING("Funds", "What fund ID are you referring to?"),
    SALESPERSON_STRING("Salesperson", "Please specify the salesperson's ID."),
    CLIENT_STRING("Clients", "Which client are you referring to?"),
    TRADE_STATUS_STRING("TradeStatus", "Which trade status are you referring to?"),
    ETC_PROTOCOL_STRING("ETCProtocol", "Which ETC protocol are you referring to?"),
    MARKET_STRING("Market", "Which market are you referring to?"),
}