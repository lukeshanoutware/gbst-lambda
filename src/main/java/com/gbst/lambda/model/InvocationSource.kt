package com.gbst.lambda.model

enum class InvocationSource {
    FulfillmentCodeHook, DialogCodeHook
}
