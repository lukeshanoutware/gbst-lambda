package com.gbst.lambda.model

data class SlotDetail(var resolutions: List<ResolutionValue>? = null,
                      var originalValue: String? = null)