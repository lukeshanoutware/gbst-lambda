package com.gbst.lambda.model

data class Message(val contentType: ContentType,
                   val content: String) {

    companion object {

        fun plainText(content: String): Message = Message(ContentType.PlainText, content)

        fun ssml(content: String): Message = Message(ContentType.SSML, """<speak>$content</speak>""")
    }
}
