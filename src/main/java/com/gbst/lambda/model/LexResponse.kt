package com.gbst.lambda.model

data class LexResponse(var dialogAction: DialogAction,
                       var sessionAttributes: Map<String, String>? = null) {

    companion object {

        fun createCloseResponse(lexRequest: LexRequest, fulfillmentState: FulfillmentState, messageContent: String): LexResponse =
                LexResponse(DialogActionClose.create(fulfillmentState, messageContent), lexRequest.sessionAttributes)

        fun createSlotResponse(lexRequest: LexRequest, messageContent: String, nextRequiredSlotName: String): LexResponse =
                LexResponse(DialogActionElicitSlot.create(messageContent, lexRequest.currentIntent.name, lexRequest.currentIntent.slots, nextRequiredSlotName), lexRequest.sessionAttributes)
    }
}