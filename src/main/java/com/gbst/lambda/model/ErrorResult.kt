package com.gbst.lambda.model

import com.gbst.lambda.jdbc.DatabaseOperationResult
import java.lang.Exception

data class ErrorResult(val exception: Exception) : DatabaseOperationResult()