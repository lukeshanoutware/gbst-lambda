package com.gbst.lambda.model

data class DialogActionElicitSlot(val message: Message,
                                  val intentName: String,
                                  val slots: Map<String, String?>?,
                                  val slotToElicit: String,
                                  val responseCard: ResponseCard? = null) : DialogAction(Type.ElicitSlot) {

    companion object {

        fun create(messageContent: String, intentName: String, slots: Map<String, String?>?, slotToElicit: String): DialogActionElicitSlot =
                DialogActionElicitSlot(Message.ssml(messageContent), intentName, slots, slotToElicit, null)
    }
}
