package com.gbst.lambda.model

data class Bot(val name: String = "",
               val version: String = "",
               val alias: String? = null)