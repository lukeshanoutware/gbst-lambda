package com.gbst.lambda.model

enum class Type {
    ElicitIntent, ElicitSlot, ConfirmIntent, Delegate, Close
}
