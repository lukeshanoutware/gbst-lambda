package com.gbst.lambda.model

data class DialogActionClose(val fulfillmentState: FulfillmentState,
                             val message: Message,
                             val responseCard: ResponseCard? = null) : DialogAction(Type.Close) {

    companion object {

        fun create(fulfillmentState: FulfillmentState, messageContent: String): DialogActionClose =
                DialogActionClose(fulfillmentState, Message.ssml(messageContent), null)
    }
}
