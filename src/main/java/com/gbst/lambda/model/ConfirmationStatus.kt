package com.gbst.lambda.model

enum class ConfirmationStatus {
    None, Confirmed, Denied
}
