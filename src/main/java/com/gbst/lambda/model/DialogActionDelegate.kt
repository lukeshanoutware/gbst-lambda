package com.gbst.lambda.model

data class DialogActionDelegate(var slots: Map<String, String?>? = null) : DialogAction(Type.Delegate)