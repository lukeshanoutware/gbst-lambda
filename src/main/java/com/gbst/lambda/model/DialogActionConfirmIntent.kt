package com.gbst.lambda.model

data class DialogActionConfirmIntent(var message: Message? = null,
                                     var intentName: String? = null,
                                     var slots: Map<String, String?>? = null,
                                     var responseCard: ResponseCard? = null) : DialogAction(Type.ConfirmIntent)