package com.gbst.lambda.model

data class Intent(var name: String = "",
                  var slots: Map<String, String?>? = null,
                  var slotDetails: Map<String, SlotDetail>? = null,
                  var confirmationStatus: ConfirmationStatus = ConfirmationStatus.None)