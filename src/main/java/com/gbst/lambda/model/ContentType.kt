package com.gbst.lambda.model

enum class ContentType {
    PlainText, SSML, CustomPayload
}
