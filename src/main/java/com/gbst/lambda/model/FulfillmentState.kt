package com.gbst.lambda.model

enum class FulfillmentState {
    Fulfilled, Failed
}
